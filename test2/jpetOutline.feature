Feature: JpetStore Demo website for people
 
   Scenario Outline: We want to connecte to JPetStore Demo website 
    	Given We have opened a firefox browser
			When We search for JPetStore Demo Website
			Then We connecte on JPetStore Demo website
			When We log in with our "<login>" login
			And We log in with our "<password>" password
			Then We are successfully logged in
			And The welcome message is dispayed

			Examples:
			| login | password | username |
			| j2ee  | j2ee     | ABC      |
			| ACID  | ACID     | ABC      |